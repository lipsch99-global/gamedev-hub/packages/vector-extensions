# Vector Extensions

## Functions

```cs
Vector2[] ToVector2Array(this Vector3[] a)
```
This Function converts a ```Vector3``` Array to a ```Vector2``` Array.


```cs
Vector3[] ToVector3Array(this Vector2[] a)
```
This Function converts a ```Vector2``` Array to a ```Vector3``` Array.


```cs
Vector3[] ToVector3Array(this Vector2[] a)
```
This Function converts a ```Vector2``` Array to a ```Vector3``` Array.


```cs
Vector2 Rotate(this Vector2 v, float degrees)
```
This Function rotates a ```Vector2``` on its Z axis.


```cs
bool IsInRange(this Vector2 a, Vector2 b, float range)
```
This Function checks wheter a ```Vector2``` is in range of another ```Vector2```.