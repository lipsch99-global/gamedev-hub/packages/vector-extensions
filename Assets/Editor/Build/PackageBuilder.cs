using UnityEditor;

public static class PackageBuilder
{
    private const string PathToPackage = "Assets/Vector Extensions/Scripts";
    private const string PackageName = "Builds/Vector-Extensions.unitypackage";

    public static void BuildPackage()
    {
        AssetDatabase.ExportPackage(PathToPackage, PackageName, ExportPackageOptions.IncludeDependencies | ExportPackageOptions.Recurse);
    }
}
