﻿using Lipsch.VectorExtensions;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace Tests
{
    public class VectorExtensionTest
    {
        [Test]
        public void ToVector2Array()
        {
            Vector3[] a = new Vector3[]
            {
                new Vector3(15,5,10),
                new Vector3(10,5,15),
                new Vector3(15,10,5)
            };

            Vector2[] b = a.ToVector2Array();

            Assert.AreEqual(15, b[0].x);
            Assert.AreEqual(5, b[0].y);
            Assert.AreEqual(10, b[1].x);
            Assert.AreEqual(5, b[1].y);
            Assert.AreEqual(15, b[2].x);
            Assert.AreEqual(10, b[2].y);
        }

        [Test]
        public void ToVector3Array()
        {
            Vector2[] a = new Vector2[]
            {
                new Vector3(15,5),
                new Vector3(10,5),
                new Vector3(15,10)
            };

            Vector3[] b = a.ToVector3Array();

            Assert.AreEqual(15, b[0].x);
            Assert.AreEqual(5, b[0].y);
            Assert.AreEqual(0, b[0].z);
            Assert.AreEqual(10, b[1].x);
            Assert.AreEqual(5, b[1].y);
            Assert.AreEqual(0, b[1].z);
            Assert.AreEqual(15, b[2].x);
            Assert.AreEqual(10, b[2].y);
            Assert.AreEqual(0, b[2].z);
        }

        [Test]
        public void Rotate()
        {
            Vector2 a = Vector2.up;
            a = a.Rotate(90);

            Assert.AreEqual(1, (int)a.x);
            Assert.AreEqual(0, (int)a.y);
        }

        [Test]
        public void VectorClockwiseComparer()
        {
            List<Vector2> a = new List<Vector2>()
            {
                Vector2.left,
                Vector2.right,
                Vector2.up,
                Vector2.down
            };

            VectorClockwiseComparer comparer = new VectorClockwiseComparer(Vector2.zero);

            a.Sort(comparer);

            Assert.AreEqual(-1, a[0].x);
            Assert.AreEqual(0, a[0].y);
            Assert.AreEqual(0, a[1].x);
            Assert.AreEqual(1, a[1].y);
            Assert.AreEqual(1, a[2].x);
            Assert.AreEqual(0, a[2].y);
            Assert.AreEqual(0, a[3].x);
            Assert.AreEqual(-1, a[3].y);
        }

    }
}
